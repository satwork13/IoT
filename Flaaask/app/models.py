from views import db

class temp(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    temp = db.Column(db.Integer)
    pub_date = db.Column(db.DateTime)

    def __init__(self,temp):
        self.temp = temp
        self.pub_date = datetime.now()


    def __repr__(self):
        return '<Temp id %r>' % (self.id)
