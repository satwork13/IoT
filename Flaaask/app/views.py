from flask import Flask,render_template,request
from flask.ext.sqlalchemy import SQLAlchemy
import serial, threading
import SerialThread, os

basedir = os.path.abspath(os.path.dirname("/home/ab/MEGA/MEGAsync/Projects-2016/NIT-IoT/IoT/Flaaask/app"))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

app=Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
db = SQLAlchemy(app)
import models

print basedir

port = '/dev/ttyACM0'
baud = 115200

serial_port = serial.Serial(port, baud, timeout=0)
filename = "/home/ab/Desktop/tempdata.dat"
target = open(filename, 'w')

thread = threading.Thread(target=SerialThread.read_from_port, args=(serial_port,target))
thread.start()

@app.route('/temp/<para>')
def branch(para):
    # Please heat printer to <para> degrees
    return para + " lolrelax"

@app.route('/',methods=['POST','GET'])
def index():
    if request.method=='POST':
        value_backend=request.form.get('value')
        value_backend2=request.form.get('value2')
        value_backend3=request.form.get('value3')
        return value_backend + value_backend2 + value_backend3
    return render_template('index.html')

@app.route('/temp',methods=['POST','GET'])
def temp():
    if request.method=='POST':
        # value_backend=request.form.get('value')
        return 0
    # Read from file here
    return render_template('temp.html')

@app.route('/turn',methods=['POST','GET'])
def turns():
    if request.method=='POST':
        direction=request.form.get('dir')
        angle=request.form.get('angle')
        ser = serial.Serial('/dev/ttyACM1', 115200) # Establish the connection on a specific port
        serialwrite = direction + "," + angle
        ser.write(str(serialwrite))
        serialdata = []
        for each in range(1,7):
            serialdata.append(ser.readline())
        print serialdata
        return render_template('turn.html',serialdata=serialdata)
    return render_template('turn.html')
